SELECT DISTINCT site, algorithm_type
FROM analysis.vis_dicts
WHERE insertion_time >= dateadd(day, -500, getdate());