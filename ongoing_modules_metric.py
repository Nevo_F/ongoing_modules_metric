import os
import glob
import pandas as pd
import warnings

PATH_OF_SITES_DEPLOYMENTS_CONFIG = os.path.abspath(glob.glob('**/sites_deployments_config.csv')[0])
PATH_OF_THE_PAST_50_DAYS_VIS_DICTS_UNIQUE_MODULES = \
    os.path.abspath(glob.glob('**/productiondb_analysis_vis_dicts_all_analyzed_modules_50_days_back.csv')[0])
PATH_OF_ALGORITHM_NAMES = os.path.abspath(glob.glob('**/algorithm_names.csv')[0])
NAMES_TO_BE_COMPARED = ['analysis_vis_dicts_name', 'ai_project_name']
IRRELEVANT_SITES = ['staging']
# IRRELEVANT_ALGORITHMS = ['CT_CSpine_Fractures_Axial']

def drop_irrelevant_values(df):
    # df = df[~df['algorithm_type'].isin(IRRELEVANT_ALGORITHMS)]
    df = df[~df['site'].isin(IRRELEVANT_SITES)]
    return df

def get_set_of_vis_dicts_modules_per_site(df: pd.DataFrame) -> set:
    return set(df.drop(columns=['#']).itertuples(index=False, name=None))


def fix_vis_dicts_names(vis_dicts_df, names_df) -> set:
    names_df = names_df[NAMES_TO_BE_COMPARED].set_index([NAMES_TO_BE_COMPARED[0]]).dropna()
    vis_dicts_df['algorithm_type'] = \
        list(names_df.loc[vis_dicts_df['algorithm_type']][NAMES_TO_BE_COMPARED[1]])
    return vis_dicts_df


def merge_columns_of_the_same_algo_type(df: pd.DataFrame) -> pd.DataFrame:
    df = df.groupby(lambda x: x.split('.')[0], axis=1).sum()
    df = df.groupby(lambda x: x.split('/')[0], axis=1).sum()
    return df != 0


def get_set_of_deployed_modules_per_site(df: pd.DataFrame) -> set:
    edited_df = df.drop([0, 1])
    edited_df = edited_df.rename(columns={'briefcase_config_key': 'site'})
    edited_df = edited_df.set_index('site')
    edited_df = edited_df.notnull()
    redundant_df = merge_columns_of_the_same_algo_type(edited_df)
    set_of_index = set(redundant_df[redundant_df].stack().index)
    return set_of_index

if __name__ == '__main__':
    names = pd.read_csv(PATH_OF_ALGORITHM_NAMES)
    vis_dicts_modules = pd.read_csv(PATH_OF_THE_PAST_50_DAYS_VIS_DICTS_UNIQUE_MODULES)
    vis_dicts_modules = drop_irrelevant_values(vis_dicts_modules)
    df_of_analyzed_modules = fix_vis_dicts_names(vis_dicts_modules, names)
    set_of_analyzed_modules = get_set_of_vis_dicts_modules_per_site(df_of_analyzed_modules)
    config_df = pd.read_csv(PATH_OF_SITES_DEPLOYMENTS_CONFIG)
    set_of_config_modules = get_set_of_deployed_modules_per_site(config_df)
    if set_of_config_modules.intersection(set_of_analyzed_modules) == set_of_analyzed_modules:
        pd.DataFrame(set_of_analyzed_modules, columns=['site','algo']).to_csv('site_vs_algo.csv')
