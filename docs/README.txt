metric name: ongoing_modules

cerator: Nevo F.

date: 19-08-21

description: This metric output is a set of tuples of the form (@site, @algorithm) and its objective is to list all the
current deployd modules. the metric measurement is based on a query of all the scans that were analyzed in the past 30
days. The databases that we use are vis_dicts (for the mentioned query) and site_deployd_config to double-check that
every module in the list is a one that we acknowledge to be deployd in the mentioned site.

remarks: In the future this metric will be based on information from the salesforce or consul (or any other relevant
platform of the product teams (since the current method to measure it now seems not to be the best one neither the
right way, although it is probably reliable).